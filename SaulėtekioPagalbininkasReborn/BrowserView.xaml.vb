﻿Imports System.Text.RegularExpressions
''' <summary>
''' An empty page that can be used on its own or navigated to within a Frame.
''' </summary>
Public NotInheritable Class BrowserView
    Inherits Page

    Private myParams As BrowserViewParameters
    Private Shared blockHtml As String = "<div style='font-size: 2em; font-family: Arial; text-align center;'>
<p style='text-align center; font-size: 3em;'><span style='color: #ff0000;'><strong>🚫 Prieiga uždrausta. </strong> </span></p>
<p style='text-align center;'><span style='color: #ff0000;'>Pra&scaron;ome naudotis bibliotekos kompiuteriais, kad tęsti nar&scaron;ymą.</span></p>
</div>"

    Private Sub MainWeb_NavigationStarting(sender As WebView, args As WebViewNavigationStartingEventArgs) Handles MainWeb.NavigationStarting
        If args IsNot Nothing And args.Uri IsNot Nothing Then
            If Not isUriAllowed(args.Uri) Then
                Debug.Write("Stoped navigation to ")
                Debug.WriteLine(args.Uri)
                Try
                    MainWeb.Stop()
                    MainWeb.NavigateToString(blockHtml)
                Catch ex As Exception

                End Try
            End If
            UpdateUrl(args.Uri.AbsoluteUri)
        End If
    End Sub

    Private Sub UpdateUrl(url As String)
        If urlBar IsNot Nothing Then
            urlBar.Text = url
        End If
    End Sub

    Private Sub MainWeb_NavigationCompleted(sender As WebView, args As WebViewNavigationCompletedEventArgs) Handles MainWeb.NavigationCompleted
        DirectCast(App.Current, App).ZeroCounter()
        If args IsNot Nothing And args.Uri IsNot Nothing Then
            If Not isUriAllowed(args.Uri) Then
                MainWeb.Stop()
                MainWeb.NavigateToString(blockHtml)
                'This time fail hard if it fails to stop navigation
            End If
            UpdateUrl(args.Uri.AbsoluteUri)
        End If
        UpdateState()
    End Sub

    Private Sub BackBTN_Click(sender As Object, e As RoutedEventArgs) Handles BackBTN.Click
        If MainWeb.CanGoBack Then
            MainWeb.GoBack()
        ElseIf Frame.CanGoBack() Then
            MainWeb.NavigateToString("")
            Frame.GoBack()
        End If
        UpdateState()
    End Sub

    Private Sub FwBTN_Click(sender As Object, e As RoutedEventArgs) Handles FwBTN.Click
        If MainWeb.CanGoForward Then
            MainWeb.GoForward()
        End If
        UpdateState()
    End Sub

    Private Sub UpdateState()
        If MainWeb IsNot Nothing And BackBTN IsNot Nothing And BackBTN IsNot Nothing Then
            BackBTN.IsEnabled = MainWeb.CanGoBack Or Frame.CanGoBack
            FwBTN.IsEnabled = MainWeb.CanGoForward
        End If
    End Sub

    Private Sub MainWeb_NewWindowRequested(sender As WebView, args As WebViewNewWindowRequestedEventArgs) Handles MainWeb.NewWindowRequested
        args.Handled = True
        Frame.Navigate(GetType(BrowserView), New BrowserViewParameters(args.Uri.AbsoluteUri, myParams.urlBarShown, myParams.allowedDomains))
    End Sub

    Protected Overrides Sub OnNavigatedTo(args As NavigationEventArgs)
        If args.Parameter.GetType Is GetType(BrowserViewParameters) Then
            myParams = args.Parameter
            MainWeb.Navigate(New Uri(myParams.url))
            If Not myParams.urlBarShown Then
                urlBar.Visibility = Visibility.Collapsed
            End If
        Else
            Throw New Exception("Failed to get Parameters")
        End If
    End Sub

    Private Sub BrowserView_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        UpdateState()
    End Sub

    Private Function isUriAllowed(uri As Uri) As Boolean
        If myParams.allowedDomains Is Nothing Then
            Return True
        End If
        Dim r = New Regex("(\w{2,63}.\w{2,63}$)")
        Dim m = r.Match(uri.Host)
        Dim domain = m.Groups.Item(0).ToString()
        If Not myParams.allowedDomains.Contains(domain) Then
            Return False
        End If
        Return True
    End Function
End Class
