﻿Public Class BrowserViewParameters
    Public url As String
    Public urlBarShown As Boolean = True
    Public allowedDomains As List(Of String)

    Public Sub New()
        Url = "about:blank"
        allowedDomains = Nothing
    End Sub

    Public Sub New(url As String)
        MyClass.New()
        Me.Url = url
    End Sub

    Public Sub New(url As String, urlBar As Boolean)
        MyClass.New(url)
        urlBarShown = urlBar
    End Sub

    Public Sub New(url As String, urlBar As Boolean, allowed As IEnumerable(Of String))
        MyClass.New(url, urlBar)
        If allowed IsNot Nothing Then
            allowedDomains = New List(Of String)
            allowedDomains.AddRange(allowed)
        End If
    End Sub

End Class
