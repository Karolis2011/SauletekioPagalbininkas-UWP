﻿' The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

''' <summary>
''' An empty page that can be used on its own or navigated to within a Frame.
''' </summary>
Public NotInheritable Class MainPage
    Inherits Page



    Private Sub homeBtn_Click(sender As Object, e As RoutedEventArgs) Handles homeBtn.Click
        Frame.Navigate(GetType(BrowserView), New BrowserViewParameters("http://sauletekis.lt/", True,
                                                                      {"sauletekis.lt",
                                                                      "siauliai.lt",
                                                                      "rasosp.lt",
                                                                      "tunevienas.lt",
                                                                      "emokykla.lt",
                                                                      "geguziai.lt",
                                                                      "su.lt",
                                                                      "jovaras.com",
                                                                      "rekyvospm.com",
                                                                      "juventa.lt",
                                                                      "bubiai.lt",
                                                                      "vdu.lt",
                                                                      "slk.lt",
                                                                      "svako.lt",
                                                                      "ktu.edu",
                                                                      "moksleiviai.lt",
                                                                      "smm.lt",
                                                                      "pvc.lt",
                                                                      "kkarolis.lt"
                                                                      }
))
    End Sub

    Private Sub sheBtn_Click(sender As Object, e As RoutedEventArgs) Handles sheBtn.Click
        Frame.Navigate(GetType(BrowserView), New BrowserViewParameters("http://sauletekis.lt/tvark/#/", False, {"sauletekis.lt"}))
    End Sub

    Private Sub jrnBtn_Click(sender As Object, e As RoutedEventArgs) Handles jrnBtn.Click
        Dim localSettings = Windows.Storage.ApplicationData.Current.LocalSettings
        Select Case localSettings.Values.Item("Buses")
            Case "S"
                Frame.Navigate(GetType(BrowserView), New BrowserViewParameters("http://www.stops.lt/siauliai/test.html", False, {"stops.lt"}))
            Case Else
                Frame.Navigate(GetType(BrowserView), New BrowserViewParameters("https://www.trafi.com/lt/siauliai/", False, {"trafi.com"}))
        End Select

    End Sub

    Private Sub Settings_Click(sender As Object, e As RoutedEventArgs) Handles Settings.Click
        Frame.Navigate(GetType(Settings))
    End Sub
End Class
