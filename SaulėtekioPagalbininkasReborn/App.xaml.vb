﻿Imports Windows.ApplicationModel.Core
Imports Windows.UI.Core
''' <summary>
''' Provides application-specific behavior to supplement the default Application class.
''' </summary>
NotInheritable Class App
    Inherits Application

    Private timerCounter As Integer = 0
    Private Const timeout As Integer = 60
    ''' <summary>
    ''' Invoked when the application is launched normally by the end user.  Other entry points
    ''' will be used when the application is launched to open a specific file, to display
    ''' search results, and so forth.
    ''' </summary>
    ''' <param name="e">Details about the launch request and process.</param>
    Protected Overrides Sub OnLaunched(e As Windows.ApplicationModel.Activation.LaunchActivatedEventArgs)
        Dim rootFrame As Frame = TryCast(Window.Current.Content, Frame)

        ' Do not repeat app initialization when the Window already has content,
        ' just ensure that the window is active
        CheckSetDefaults()
        If rootFrame Is Nothing Then
            ' Create a Frame to act as the navigation context and navigate to the first page
            rootFrame = New Frame()

            AddHandler rootFrame.NavigationFailed, AddressOf OnNavigationFailed
            AddHandler rootFrame.PointerMoved, AddressOf ZeroCounter
            AddHandler rootFrame.Tapped, AddressOf ZeroCounter
            AddHandler rootFrame.RightTapped, AddressOf ZeroCounter

            If e.PreviousExecutionState = ApplicationExecutionState.Terminated Then
                ' TODO: Load state from previously suspended application
            End If
            ' Place the frame in the current Window
            Window.Current.Content = rootFrame
        End If

        If e.PrelaunchActivated = False Then
            If rootFrame.Content Is Nothing Then
                ' When the navigation stack isn't restored navigate to the first page,
                ' configuring the new page by passing required information as a navigation
                ' parameter
                rootFrame.Navigate(GetType(MainPage), e.Arguments)
            End If

            ' Ensure the current window is active
            Window.Current.Activate()
        End If
        SetupTimer()
    End Sub

    ''' <summary>
    ''' Invoked when Navigation to a certain page fails
    ''' </summary>
    ''' <param name="sender">The Frame which failed navigation</param>
    ''' <param name="e">Details about the navigation failure</param>
    Private Sub OnNavigationFailed(sender As Object, e As NavigationFailedEventArgs)
        Throw New Exception("Failed to load Page " + e.SourcePageType.FullName)
    End Sub

    ''' <summary>
    ''' Invoked when application execution is being suspended.  Application state is saved
    ''' without knowing whether the application will be terminated or resumed with the contents
    ''' of memory still intact.
    ''' </summary>
    ''' <param name="sender">The source of the suspend request.</param>
    ''' <param name="e">Details about the suspend request.</param>
    Private Sub OnSuspending(sender As Object, e As SuspendingEventArgs) Handles Me.Suspending
        Dim deferral As SuspendingDeferral = e.SuspendingOperation.GetDeferral()
        ' TODO: Save application state and stop any background activity
        deferral.Complete()
    End Sub

    Private Sub SetupTimer()
#Disable Warning BC42358 ' Because this call is not awaited, execution of the current method continues before the call is completed
        CoreApplication.GetCurrentView().Dispatcher.RunAsync(CoreDispatcherPriority.Normal, Async Sub()
                                                                                                While (True)
                                                                                                    Await Task.Delay(1000)
                                                                                                    clickTimer_Tick()
                                                                                                End While
                                                                                            End Sub)
#Enable Warning BC42358 ' Because this call is not awaited, execution of the current method continues before the call is completed
    End Sub

    Friend Sub ZeroCounter()
        timerCounter = 0
    End Sub

    Private Sub clickTimer_Tick()
        timerCounter += 1
        If timerCounter > timeout Then
            Dim frame As Frame = Window.Current.Content
            If frame.CurrentSourcePageType IsNot GetType(MainPage) And frame.CurrentSourcePageType IsNot GetType(MediaPlayer) Then
                frame.Navigate(GetType(MainPage))
                frame.BackStack.Clear()
                frame.ForwardStack.Clear()
            End If
            timerCounter = 0
        End If

    End Sub

    Private Sub CheckSetDefaults()
        Dim localSettings = Windows.Storage.ApplicationData.Current.LocalSettings
        If localSettings.Values.Item("ExitCode") IsNot Nothing Then
            Return
        End If
        localSettings.Values.Item("ExitCode") = "72491912"
        localSettings.Values.Item("MaintenanceCode") = "67291189"
        localSettings.Values.Item("MediaCode") = "2468"
        localSettings.Values.Item("Buses") = "T"
    End Sub

End Class
