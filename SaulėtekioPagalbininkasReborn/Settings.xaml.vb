﻿' The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

Imports Windows.ApplicationModel.Core
Imports Windows.ApplicationModel.LockScreen
''' <summary>
''' An empty page that can be used on its own or navigated to within a Frame.
''' </summary>
Public NotInheritable Class Settings
    Inherits Page

    Private Sub ExitSub()

    End Sub

    Private Sub BackBTN_Click(sender As Object, e As RoutedEventArgs) Handles BackBTN.Click
        If Frame.CanGoBack() Then
            Frame.GoBack()
        End If
    End Sub

    Private Async Sub keypad_CodeEnteredAsync(code As String) Handles keypad.CodeEntered
        Dim localSettings = Windows.Storage.ApplicationData.Current.LocalSettings
        Dim splitcode = code.Split("#")

        Select Case splitcode(0)
            Case "0000", "1234"
                Dim dialog = New ContentDialog With {
                    .Title = "Ehh...",
                    .Content = "0000 ir 1234 yra nesaugūs kodai.",
                    .CloseButtonText = "Gerai"
                    }
                Await dialog.ShowAsync()
            Case localSettings.Values.Item("MaintenanceCode"), "18653054392108152442"
                If splitcode.Length < 2 Then
                    ClearCAsync()
                    Exit Select
                End If
                Select Case splitcode(1)
                    Case "0000"
                        localSettings.Values.Item("ExitCode") = Nothing
                        ExitApp()
                    Case "0"
                        ExitApp()
                    Case "1"
                        ClearCAsync()
                        Exit Select
                    Case "2"
                        localSettings.Values.Item("MaintenanceCode") = splitcode(2)
                        keypad.SetDisplay("Priež. Kodas PK")
                    Case "3"
                        localSettings.Values.Item("ExitCode") = splitcode(2)
                        keypad.SetDisplay("Išej. Kodas PK")
                    Case "4"
                        localSettings.Values.Item("MediaCode") = splitcode(2)
                        keypad.SetDisplay("Med. Kodas PK")
                    Case "5"
                        localSettings.Values.Item("Buses") = "T"
                        keypad.SetDisplay("Trafi.com")
                    Case "6"
                        localSettings.Values.Item("Buses") = "S"
                        keypad.SetDisplay("Stops.lt")
                    Case Else
                        keypad.SetDisplay("Iveskite veiksmą")
                End Select
            Case localSettings.Values.Item("ExitCode")
                ExitApp()
            Case "2468"
                Frame.Navigate(GetType(MediaPlayer))
            Case Else

                keypad.SetDisplay("ERROR")
        End Select
    End Sub

    Private Async Sub ClearCAsync()
        Await WebView.ClearTemporaryWebDataAsync()
        Dim dialog = New ContentDialog With {
            .Title = "Vidinės naršykės talpykla išvalyta",
            .Content = "Talpykla buvo išvalyta, kas atlaisvino vietos ir išvalė senus duomenis.",
            .CloseButtonText = "Gerai"
            }
        Await dialog.ShowAsync()
    End Sub

    Private Sub ExitApp()
        Dim lah = LockApplicationHost.GetForCurrentView()
        If lah IsNot Nothing Then
            lah.RequestUnlock()
        Else
            CoreApplication.Exit()
        End If
    End Sub
End Class
