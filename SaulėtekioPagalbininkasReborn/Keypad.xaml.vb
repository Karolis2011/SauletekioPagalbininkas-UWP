﻿' The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

Public NotInheritable Class Keypad
    Inherits UserControl

    Private _code As String = ""

    Public Event CodeEntered(code As String)

    Private Sub KeyPress(key As Char)
        _code += key
        UpdateDisplay()
    End Sub

    Private Sub _Btn_Click(sender As Object, e As RoutedEventArgs) Handles _1Btn.Click, _2Btn.Click, _3Btn.Click, _4Btn.Click, _5Btn.Click, _6Btn.Click, _7Btn.Click, _8Btn.Click, _9Btn.Click, _0Btn.Click, _HashBtn.Click
        DirectCast(App.Current, App).ZeroCounter()
        Dim btn = TryCast(sender, Button)
        KeyPress(btn.Content)
    End Sub

    Private Sub _EntBtn_Click(sender As Object, e As RoutedEventArgs) Handles _EntBtn.Click
        Dim codecopy = _code
        _code = ""
        UpdateDisplay()
        RaiseEvent CodeEntered(codecopy)
    End Sub

    Public Sub ClearCode() Handles _RSTBtn.Click
        If _code.Length > 0 Then
            _code = _code.Substring(0, _code.Length - 1)
        End If
        UpdateDisplay()
    End Sub

    Private Sub UpdateDisplay()
        StatusLbl.Text = _code
    End Sub

    Public Sub SetDisplay(text As String)
        StatusLbl.Text = text
    End Sub

    Private Sub UserControl_Loading(sender As FrameworkElement, args As Object)
        UpdateDisplay()
    End Sub
End Class
