﻿' The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

Imports Windows.Media.Core
Imports Windows.Media.Playback
''' <summary>
''' An empty page that can be used on its own or navigated to within a Frame.
''' </summary>
Public NotInheritable Class MediaPlayer
    Inherits Page

    Private Async Sub OpenBtn_ClickAsync(sender As Object, e As RoutedEventArgs) Handles OpenBtn.Click
        Dim pick = New Windows.Storage.Pickers.FileOpenPicker()
        pick.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail
        pick.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.ComputerFolder
        pick.FileTypeFilter.Add(".mp3")
        pick.FileTypeFilter.Add(".wav")
        pick.FileTypeFilter.Add(".mp4")
        pick.FileTypeFilter.Add(".mov")

        Dim File = Await pick.PickSingleFileAsync()
        If File IsNot Nothing Then
            player.Source = MediaSource.CreateFromStorageFile(File)
        End If
    End Sub

    Private Sub BackBTN_Click(sender As Object, e As RoutedEventArgs) Handles BackBTN.Click
        If Frame.CanGoBack() Then
            Frame.GoBack()
        End If
    End Sub
End Class
